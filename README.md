# README #

OPENCV NAI PROJECT

### What is this repository for? ###

* This repo is for a project for NAI class. It will be a some kind of system to open apps/programs with gestures made by some object captured by some camera.
* It uses an OpenCv technology for Computer Vision related matters.(Pain in the ass to adjust color to recognize in different lighting)
* As for the gesture recognition, all the code related to registering and recognising a gesture was completely mine.

### How do I get set up? ###

* All the compiling was made on a mac machine. So for now it is supposed to open mac apps.
* Here is my trello board that shows the progress and steps that I made to make this work: "https://trello.com/b/FL6ipsNG/nai-project". 
 
### Contribution guidelines ###

* All done by Michał Wołowiec