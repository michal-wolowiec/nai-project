#include <iostream>
#include <vector>

using namespace std;

namespace sha{

//1 LINE GESTURES

bool isVerticalLine(vector<int> recVecV, vector<int> recVecH){
	vector<int> vecV(1);
	vecV[0] = 8;

	vector<int> vecH(1);
	vecH[0] = 0;

	if((recVecV == vecV) && (recVecH == vecH)) return true;
	else return false;
}

bool isHorizontalLine(vector<int> recVecV, vector<int> recVecH){
	vector<int> vecV(1);
	vecV[0] = 0;

	vector<int> vecH(1);
	vecH[0] = 6;

	if((recVecV == vecV) && (recVecH == vecH)) return true;
	else return false;
}

//2 LINE GESTURES

	bool isLetterC(vector<int> recVecV, vector<int> recVecH){
		vector<int> vecV(2);
		vector<int> vecH(2);

		vecV[0] = 2;
		vecV[1] = 2;

		vecH[0] = 4;
		vecH[1] = 6;

		if((recVecV == vecV) && (recVecH == vecH)) return true;
		else return false;
	}

	bool isLetterA(vector<int> recVecV, vector<int> recVecH){
		vector<int> vecV(2);
		vector<int> vecH(2);

		vecV[0] = 8;
		vecV[1] = 2;

		vecH[0] = 6;
		vecH[1] = 6;

		if((recVecV == vecV) && (recVecH == vecH)) return true;
		else return false;
	}

//3 LINE GESTURES

	bool isLetterD(vector<int> recVecV, vector<int> recVecH){
		vector<int> vecV(3);
		vector<int> vecH(3);

		vecV[0] = 8;
		vecV[1] = 2;
		vecV[2] = 2;

		vecH[0] = 0;
		vecH[1] = 6;
		vecH[2] = 4;

		if((recVecV == vecV) && (recVecH == vecH)) return true;
		else return false;
	}

	bool isTriangle(vector<int> recVecV, vector<int> recVecH){
		vector<int> vecV(3);
		vector<int> vecH(3);

		vecV[0] = 8;
		vecV[1] = 2;
		vecV[2] = 0;

		vecH[0] = 6;
		vecH[1] = 6;
		vecH[2] = 4;

		if((recVecV == vecV) && (recVecH == vecH)) return true;
		else return false;
	}

//4 LINE GESTURES

	bool isSquare(vector<int> recVecV, vector<int> recVecH){
		vector<int> vecV(4);
		vector<int> vecH(4);

		vecV[0] = 8;
		vecV[1] = 0;
		vecV[2] = 2;
		vecV[3] = 0;

		vecH[0] = 0;
		vecH[1] = 6;
		vecH[2] = 0;
		vecH[3] = 4;

		if((recVecV == vecV) && (recVecH == vecH)) return true;
		else return false;
	}

	bool isHorizontalArrow(vector<int> recVecV, vector<int> recVecH){
		vector<int> vecV(4);
		vector<int> vecH(4);

		vecV[0] = 0;
		vecV[1] = 8;
		vecV[2] = 2;
		vecV[3] = 2;

		vecH[0] = 6;
		vecH[1] = 4;
		vecH[2] = 6;
		vecH[3] = 4;

		if((recVecV == vecV) && (recVecH == vecH)) return true;
		else return false;
	}

	bool isVerticalArrow(vector<int> recVecV, vector<int> recVecH){
		vector<int> vecV(4);
		vector<int> vecH(4);

		vecV[0] = 8;
		vecV[1] = 2;
		vecV[2] = 8;
		vecV[3] = 2;

		vecH[0] = 0;
		vecH[1] = 4;
		vecH[2] = 6;
		vecH[3] = 6;

		if((recVecV == vecV) && (recVecH == vecH)) return true;
		else return false;
	}

	bool isRhombus(vector<int> recVecV, vector<int> recVecH){
		vector<int> vecV(4);
		vector<int> vecH(4);

		vecV[0] = 8;
		vecV[1] = 2;
		vecV[2] = 2;
		vecV[3] = 8;

		vecH[0] = 6;
		vecH[1] = 6;
		vecH[2] = 4;
		vecH[3] = 4;

		if((recVecV == vecV) && (recVecH == vecH)) return true;
		else return false;
	}



}
