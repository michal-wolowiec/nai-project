#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <vector>
#include <cmath>

#include "shapes.h"



using namespace std;
using namespace cv;
using namespace sha;





namespace det{

	int length(double x1, double y1, double x2, double y2){
		return sqrt(pow((x2-x1),2) + pow((y2-y1),2));

	}

	// Function that determines the Vertial movement direction
	int directionV(int pos, int lastPos){
	  if(pos - lastPos > 10) return 2;
	  else if(pos - lastPos < -10) return 8; 
	  	else return 0;
	}

	// FUnction that determines the Horizontal movement direction
	int directionH(int pos, int lastPos){
	  if(pos - lastPos > 10) return 6;
	    else if(pos - lastPos < -10) return 4;
	    	else return 0;
	}

	
	void analize(vector<int> recVecV, vector<int> recVecH, int lines){
		switch(lines){
			case 1:
				if(isVerticalLine(recVecV, recVecH)) {
					cout << "Vertical Line. /n Opening TeamSpeak3" << endl;
					system("open /Applications/'TeamSpeak 3 Client.app'");
				}
				else if(isHorizontalLine(recVecV, recVecH)){
					cout << "Horizontal Line. /n Opening iTunes" << endl;
					system("open /Applications/'iTunes.app'");
				}
					else cout << "Cant recognize the gesture make sure that you are 'drawing' the geture in the correct order " << lines << endl;
			break;

			case 2:
				if(isLetterC(recVecV, recVecH)){
					cout << "Letter C. /n Opening Contacts" << endl;
					system("open /Applications/'Contacts.app'");
				}
				else if(isLetterA(recVecV, recVecH)) {
					cout << "Letter A. /n Opening Adobe Acrobat Reader DC" << endl;
					system("open /Applications/'Adobe Acrobat Reader DC.app'");
				}
					else cout << "Cant recognize the gesture make sure that you are 'drawing' the geture in the correct order " << lines << endl;
			break;

			case 3: 
				if(isLetterD(recVecV, recVecH)) {
					cout << "Letter D. /n Opening DeSmuMe" << endl;
					system("open /Applications/'DeSmuMe.app'");
				}
				else if(isTriangle(recVecV, recVecH)) {
					cout << "Triangle. /n Opening VLC" << endl;
					system("open /Applications/'VLC.app'");
				}
					else cout << "Cant recognize the gesture make sure that you are 'drawing' the geture in the correct order " << lines << endl;
			break;

			case 4:
				if(isSquare(recVecV, recVecH)) {
					cout << "Square. /n Opening DOSBox" << endl;
					system("open /Applications/'DOSBox.app'");
				}
				else if(isRhombus(recVecV, recVecH)) {
					cout << "Rhombus. /n Opening Dictionary" << endl;
					system("open /Applications/'Dictionary.app'");
				}
					 else if(isVerticalArrow(recVecV, recVecH)) {
					 	cout << "Vertical Arrow. /n Opening Appstore" << endl;
					 	system("open /Applications/'App Store.app'");
					 }
					 	  else if(isHorizontalArrow(recVecV, recVecH)) {
					 	  	cout << "Horizontal Arrow. /n Opening Safari" << endl;
					 	  	system("open /Applications/'Safari.app'");
					 	  }
					 	  	   else cout << "Cant recognize the gesture make sure that you are 'drawing' the geture in the correct order " << lines <<  endl;
 			break;

			default:
				cout << "I didn't recognize the " << lines << " line gesture!!" << endl;
			break;
		}
	}


	//DEBUG FUNCTIONS //

	//trackbars to adjust HSV of the trackobject
	void addTrackbars(int &iLH, int &iHH, int &iLS, int &iHS, int &iLV, int &iHV, char* windowName){
		cvCreateTrackbar("LowH", windowName, &iLH, 179); 
 		cvCreateTrackbar("HighH", windowName, &iHH, 179);

  		cvCreateTrackbar("LowS", windowName, &iLS, 255); 
 		cvCreateTrackbar("HighS", windowName, &iHS, 255);

		cvCreateTrackbar("LowV", windowName, &iLV, 255); 
		cvCreateTrackbar("HighV", windowName, &iHV, 255);
	}

	//backup way to adjust HSV for the trakced object(if there is something wrong with trackbars)
	void menu(int &iLH, int &iHH, int &iLS, int &iHS, int &iLV, int &iHV){
		int choice;

		cout << "Make choice(lh=1, hh=2, ls=3, hs=4, lv=5, hv=6,): ";
		cin >> choice;

		switch(choice){
			case 1:
			cout << "current value: " << iLH << endl;
			cin >> iLH ;
			break;

			case 2:
			cout << "current value: " << iHH << endl;
			cin >> iHH;
			break;

			case 3:
			cout << "current value: " << iLS << endl;
			cin >> iLS;
			break;

			case 4:
			cout << "current value: " << iHS << endl;
			cin >> iHS;
			break;

			case 5:
			cout << "current value: " << iLV << endl;
			cin >> iLV;
			break;

			case 6:
			cout << "current value: " << iHV << endl;
			cin >> iHV;
			break;
		}	
	}

}
