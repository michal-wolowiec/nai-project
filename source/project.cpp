#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <ctime>

#include "detect.h"



using namespace std;
using namespace cv;





int main(){
 
  VideoCapture cap(0);  //open cam

  if(!cap.isOpened()){
    cout << "Can't open cam" << endl;
    return -1;
  }
    //Frame size : 1280 x 720
    double dWidth = cap.get(CV_CAP_PROP_FRAME_WIDTH); //get the width of frames of the video
   double dHeight = cap.get(CV_CAP_PROP_FRAME_HEIGHT); //get the height of frames of the video
  

  int iLastX = -1;
  int iLastY = -1;

 
  int iLowH = 3;
  int iHighH = 73;

  int iLowS = 163; 
  int iHighS = 255;

  int iLowV = 197;
  int iHighV = 255;
 
  // (H,S,V) for pink ball
  /*int iLowH = 0;
  int iHighH = 179;

  int iLowS = 146; 
  int iHighS = 255;

  int iLowV = 156;
  int iHighV = 235;*/

   namedWindow("Thresholded", CV_WINDOW_AUTOSIZE);

    
  //det::addTrackbars(iLowH, iHighH, iLowS, iHighS, iLowV, iHighV, "Thresholded"); // comment this line to add trackbars to adjust HSV


  Mat imgTemp;
  cap.read(imgTemp);

  //create transparent frame
  Mat imgLines = Mat::zeros(imgTemp.size(), CV_8UC3);;

  int help;
   int lines = 0;
   int linecount = 0;
   int fps = 0;
   bool still = false;
   bool inPlace = false;
   bool dot = true;

   int tempV;
   int tempH;

   vector<int> vecV;
   vector<int> vecH;


   cout << "To exit the program press 'ESC'" << endl
        << "To adjust HSV press 'j' button " << endl;


   // main loop
  while(true){
    fps++; // Second counter

    Mat imgOriginal;

    bool bSuccess = cap.read(imgOriginal);

    if(!bSuccess){
      cout << "cant open imgOriginal" << endl;
      break;
    }

    Mat imgHSV;

    cvtColor(imgOriginal, imgHSV, COLOR_BGR2HSV);

    Mat imgThresholded;

     inRange(imgHSV, Scalar(iLowH, iLowS, iLowV), Scalar(iHighH, iHighS, iHighV), imgThresholded);

     //delete image noise 
    erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );
    dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );
    
    //dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );    
    //erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );
    
    Moments oMoments = moments(imgThresholded);

    double dM01 = oMoments.m01;
    double dM10 = oMoments.m10;
    double dArea = oMoments.m00;

    // general x and y used for tracking 
    int posX;
    int posY;

    // x and y used to determine if user is holding the object still during the recording phase
    int sPosX;
    int sPosY;

    // x and y used to determine when to start or stop gesture recording 
    int startPosX;
    int startPosY;

    // x and y used to determine whether the line sthat user made was too short to register solving gesture with angles ...
    int shortPosx;
    int shortPosy;




    // save x and y after 3 sec 
    if(fps == 36){
            startPosX = posX;
            startPosY = posY;
          fps = 0;
        }

    if(fps % 4 == 0){
      sPosX = posX;
      sPosY = posY;
    }


    // if area of the detected objest is >100000 draw a dot or follow with a line in the center of it 
    if(dArea > 400000){
      
      posX = abs((dM10 / dArea)-dWidth);
      posY = dM01 / dArea;

      if(iLastX >= 0 && iLastY >=0 && posX >=0 && posY >= 0){

        if((startPosX<posX+15 && startPosX>posX-15) && (startPosY<posY+15 && startPosY>posY-15)) still = true;
        else still = false;

        if(still){
          if(dot) dot = false;
          else dot = true;
        }

        if(!dot){
          line(imgLines, Point(posX, posY), Point(iLastX, iLastY), Scalar(120,255,255), 5);
          startPosX = -1;
          startPosY = -1;
        } else {
          line(imgLines, Point(posX, posY), Point(posX, posY), Scalar(255,0,255), 10);
          startPosX = -1;
          startPosY = -1;
        }

      }
    }


    //dislay video
    flip(imgOriginal, imgOriginal, 1);
    // imshow("Thresholded", imgThresholded); //  uncomment this line to adjust HSV

    imgOriginal = imgOriginal + imgLines;
    imshow("Original", imgOriginal);
    if(dot) imgLines = Mat::zeros(imgOriginal.size(), CV_8UC3);;

    //if user is keeping the object in one place 
    bool isStill = ((sPosX<posX+15 && sPosX>posX-15) && (sPosY<posY+15 && sPosY>posY-15));

      //register the gesture and save direction to vectors
      if(!dot && !isStill){
        if((vecV.size() == 0) && (vecH.size() == 0)){
          vecV.push_back(det::directionV(posY, iLastY));
          vecH.push_back(det::directionH(posX, iLastX));

          lines++;
          linecount++;
          cout << "first H: "<< vecV[lines-1] << " V: " << vecH[lines-1] << endl;
        } else if((vecV[lines-1] != det::directionV(posY, iLastY)) || (vecH[lines-1] != det::directionH(posX, iLastX))){
          if(!((det::directionH(posX, iLastX) == 0) && (det::directionV(posY, iLastY) == 0))) {
            if(lines > linecount){
              if(det::length(posX, posY, shortPosx, shortPosy) > 50){
              vecV.push_back(tempV);
              vecH.push_back(tempH);
              cout << "length: " << det::length(posX, posY, shortPosx, shortPosy) << endl;
              cout << "next H: "<< vecV[lines-1] << " V: " << vecH[lines-1] << endl;
              linecount++;
              lines--;
              } else lines--;
            }
            shortPosx = posX;
            shortPosy = posY;
            tempH = det::directionH(posX, iLastX);
            tempV = det::directionV(posY, iLastY);
            lines++;
          }
        }
      }

      if( (dot) && (vecV.size() != 0) && (vecH.size() != 0)  ){
        det::analize(vecV, vecH, lines-1);
        vecV.clear();
        vecH.clear();
        lines = 0;
        linecount = 0;
        shortPosx = 0;
        shortPosy = 0;
      }




    //debug menu to adjust HSV
    if(waitKey(30) == 106){
      //det::menu(iLowH, iHighH, iLowS, iHighS, iLowV, iHighV);
      det::addTrackbars(iLowH, iHighH, iLowS, iHighS, iLowV, iHighV, "Original");
    }
    

    iLastX = posX;
    iLastY = posY;


    if(waitKey(30) == 27){
      cout << "iLowH: " << iLowH <<
            " iHighH: " << iHighH <<
            " iLowS: " << iLowS <<
            " iHighS: " << iHighS <<
            " iLowV: " << iLowV << 
            " iHighV: " << iHighV << endl; 
      for(int i=0;i<vecV.size();i++){
        cout << "V: " << vecV[i] << " H: " << vecH[i] << endl;
      }
      break;
    }
    

  }

  return 0;
}